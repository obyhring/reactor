//-----musikk konstanter-----
const int NOTE_1 = 165;
const int NOTE_2 = 131;
const int NOTE_3 = 196;
const int tempo = 120;

int buttonsPosition[14] = {1,1,1,1, 3,3,3,3, 2,2,2,2,2,2};
int lyder[3] = {10, 80, 200};

void peep(){  
  int wallId = buttonsPosition[buttonId];
  spilleNote(piezo[wallId - 1], lyder[wallId - 1], 1, 0);
}


void spilleNote(int piezo, int note, double lengdeNote, double lengdePause) {
  tone(piezo, note);
  delay(tempo * lengdeNote);
  noTone(piezo);
  delay(tempo * lengdePause);
}

void spilleImperialMarsh() {
  spilleNote(piezo[1], NOTE_1, 2.5, 1.5);
  spilleNote(piezo[1], NOTE_1, 2.5, 1.5);
  spilleNote(piezo[1], NOTE_1, 2.5, 1.5);
  spilleNote(piezo[1], NOTE_2, 2, 1);
  spilleNote(piezo[1], NOTE_3, 1, 0);
  spilleNote(piezo[1], NOTE_1, 2, 2);
  spilleNote(piezo[1], NOTE_2, 2, 1);
  spilleNote(piezo[1], NOTE_3, 1, 0);
  spilleNote(piezo[1], NOTE_1, 2, 2);

}