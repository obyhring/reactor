/*  the button parameter specifies what pin the button is connected to on the shift registers
 *  status is either HIGH to make the LED light up and enable the button, or LOW to turn them off
 */
void setShiftPinActive(int pin){
  //Don't do anything if there is no button on this pin
  if(pin >= 14) return;
  
  //Bit-shift to the correct placement. if we want to activate pin 4, this will create
  //bytes that looks like this: 00000000 00010000
  byte output1 = 0;
  byte output2 = 0;
  
  if(pin < 8){
    output1 = 0;                 //The second register is all 0's
    output2 = 1 << pin;       //The first register has a 1
  } else{
    output1 = 1 << (pin-8);   //The second register has a 1
    output2 = 0;                 //The first register is all 0's
  }

  //Push the bytes to the registers
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, output1);
  shiftOut(dataPin, clockPin, MSBFIRST, output2);
  digitalWrite(latchPin, HIGH);
}

void setAll(boolean active){
  //Bit-shift to the correct placement. if we want to activate pin 4, this will create
  //bytes that looks like this: 00000000 00010000
  byte output1 = 0;
  byte output2 = 0;
  
  if (active){
    output1 = 255; //11111111 - in binary
    output2 = 255;     
  }
    
  //Push the bytes to the registers
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, output1);
  shiftOut(dataPin, clockPin, MSBFIRST, output2);
  digitalWrite(latchPin, HIGH);
}