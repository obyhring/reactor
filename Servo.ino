//Uses the servo to show how many points we have scored
void displayPoints(int points){ 
  int angle = min(160/max_points*points, 160);  
  myServo.write(angle);

}
//Uses the servo to show how many lives we have left
void displayLives(){
  return;
  
  if (!typePlay == 1) {
    return;
  }
  
  int angle = min(160 - 160/max_lives*lives, 160);  
  myServo.write(angle);
}