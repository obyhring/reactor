#include <Servo.h>

//Play options
const int typePlay = 1; //0 - max points, 1 - lives countdown, 2 - limited time
const int typeButtonsOrder = 0; //0 - random, 1 - predefined
const int allowed_delay = 2200; //redusert fra 3000
int max_points = 10;
const int max_lives = 3;
const int timePlay = 1000 * 20; //20 sec

//Pins for shift register
const int latchPin = 9;
const int dataPin = 11;
const int clockPin = 12;

//Other pins
const int knapp = 2;
const int startButton = 13;
const int servoPin = 3;
const int piezo[3] = {10, 5, 6};

//Variables
int buttonId = 0;
int points = 0;
int lives = max_lives;
boolean inPlay = false;
boolean firstPeep = false;

Servo myServo;

unsigned long previousMillis_button;
const int interval_button = 200;

unsigned long allowed_delay_check;
unsigned long millisPlayStarted;

void setup() {
  //Setup shift registers
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  
  //Setup audio elements
  pinMode(piezo[0], OUTPUT);
  pinMode(piezo[1], OUTPUT);
  pinMode(piezo[2], OUTPUT);

  Serial.begin(9600);

  pinMode(knapp, INPUT);
  pinMode(startButton, INPUT);

  myServo.attach(servoPin);

  setAll(false);
  setPoints(0, true);
  myServo.write(0);

  previousMillis_button = millis();
  allowed_delay_check = millis();
  
  if (!typePlay == 0) {
    max_points = 50 ;    
  }
}

void loop() {
  //Check if we should restart the game
  if (buttonIsClicked(startButton)) {
    resetGame();
  }

  //Don't do anything if we are not playing the game!
  if (!inPlay) {
    return;
  }

  //Check if the user has pressed a button
  boolean notInTime = intervalenGikk(allowed_delay_check, allowed_delay);
  if (firstPeep || buttonIsClicked(knapp) || notInTime) {

    buttonId = getNextButton();
    Serial.print("ButtonId: ");
    Serial.println(buttonId);

    if (notInTime) {
      lives--; 
      displayLives();    
    } else if (! firstPeep) {
      setPoints(points + 1, true);
      delay(10);
      
      Serial.print("points: ");
      Serial.println(points); 
    }

    peep();
    firstPeep = false;
    
    setShiftPinActive(buttonId);
    allowed_delay_check = millis();
  }

  if (gameIsOver()) {
    endPlay();
  }
}

//Check if the game is over
boolean gameIsOver() {
  if (typePlay == 0) {
    return (points == max_points);

  } else if (typePlay == 1) {
    return (lives == 0);

  } else if (typePlay == 2) {
    return intervalenGikk(millisPlayStarted, timePlay);

  }
}

//Ends the currently active game
void endPlay() {
  inPlay = false;
  
  setPoints(0, false);
  buttonId = 0;
  
  blinkAllLeds(4);
  spilleImperialMarsh();
}

//Resets the game, so we can play again!
void resetGame() {
  setPoints(0, true);
  lives = max_lives;
  displayLives();

  buttonId = 0;
  inPlay = true;

  blinkAllLeds(1);
  firstPeep = true;

  allowed_delay_check = millis();
  millisPlayStarted = millis();
}

//Blinks all LEDs on and off
void blinkAllLeds(int times) {
  for (int n = 0; n < times; n++) {
    setAll(true);
    delay(200);

    setAll(false);
    delay(200);
  }
}

//Used to prevent flickering
boolean intervalenGikk(unsigned long previousMillis, int interval) {
  return (millis() - previousMillis >= interval);
}

//Check if the button on pin buttonPin is clicked
boolean buttonIsClicked(int buttonPin) {
  if (intervalenGikk(previousMillis_button, interval_button)) {
    if (digitalRead(buttonPin) == HIGH) {
      previousMillis_button = millis();
      return true;
    }
  }
  return false;
}

//Updates the scoreboard
void setPoints(int newPoints, boolean withDisplaying) {
  points = newPoints;
  if (withDisplaying) {
    displayPoints(points);
  }
}