//WARNING: Keep these updated AT ALL TIMES
int numberOfPatterns = 1;
int numberOfButtonsInEachPattern = 13;

//for testing of random order
int buttons_from = 0;
int buttons_to = 13;


//Holds all the valid patterns for our game.
//The function getNextButton loops through a single pattern until the end,
//then chooses a new pattern automagically. All patterns must be the same length.
//See documentation for spacial button setup.
int patterns[1][13] = {
  {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},

};

//The pattern we are currently running through
int activePattern = -1;
//The index of the button that is currently lighting up
int activePatternIndex = -1;

int getNextButton() {

  int nextButtonID = buttonId;
  
  //Random
  if (typeButtonsOrder == 0) {    
    while (nextButtonID == buttonId) {
      nextButtonID = random(buttons_from, buttons_to);
    }
    return nextButtonID;
  }

  //Predefined
  //We need to choose our next pattern and reset the index
  if (activePattern == -1) {
    activePattern = random(0, numberOfPatterns);
    activePatternIndex = 0;
  }

  //Find the ID of the button that should light up next
  nextButtonID = patterns[activePattern][activePatternIndex];

  //Move to the next button in this pattern
  activePatternIndex++;

  //Check if we have completed the current pattern
  if (activePatternIndex == numberOfButtonsInEachPattern) {
    activePattern = -1; //Choose a new pattern next time
  }

  return nextButtonID;
}